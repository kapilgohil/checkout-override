if(typeof Checkout === 'object'){
  if(typeof Checkout.$ === 'function'){
	//Set font on the body
    var head = document.getElementsByTagName("head")[0];
	head.append("<link href='https://fonts.googleapis.com/css?family=Karla' rel='stylesheet'>");
    var body = document.getElementsByTagName("body")[0];
    body.style.fontFamily = "Karla, serif"; 

	// Adding The Chapar logo into the top left corner
	var logo = document.createElement('div');
	var img = document.createElement('img');
	var content = document.querySelector('.content');
	logo.setAttribute("id", "logo");
	logo.setAttribute("style", "padding-top: 20px;");
	img.setAttribute("src", "//cdn.shopify.com/s/files/1/0008/0583/0717/files/The-Chap-Logo-01-260_100x.png?v=1542987967");
	img.setAttribute("width", "50");
	img.setAttribute("alt", "The Chapar Logo");
	logo.appendChild(img);
	content.parentNode.insertBefore(logo, content);
	document.getElementById("logo").style.top = "0";
	document.getElementById("logo").style.left = "0";
	document.getElementById("logo").style.textAlign = "center";
	document.getElementById("logo").style.width = "100%";
	document.getElementById("logo").style.background = "rgba(255,255,255,0.6)";

    // Creating header div to contain the title and the grey bar
	var header = document.createElement('div');
	var headerContent = document.createElement('div');
	var h2 = document.createElement('h2');
	var span = document.createElement('span');
	var content = document.querySelector('.content');
	header.setAttribute("id", "header");
	header.setAttribute("class", "wrap");
	header.appendChild(headerContent);
	headerContent.setAttribute("id", "headerContent");
	headerContent.appendChild(h2);
	h2.appendChild(span);
	h2.setAttribute("id", "title");
	span.setAttribute("id", "titleText");
	content.parentNode.insertBefore(header, content);
	document.getElementById("titleText").innerHTML = "Checkout";
	document.getElementById("title").style.margin = "-18px 0 35px 0";
	document.getElementById("title").style.fontSize = "1.9em";
	document.getElementById("title").style.color = "#000";
	document.getElementById("titleText").style.backgroundColor = "#FFF";
	document.getElementById("titleText").style.padding = "0 15px";
	document.getElementById("headerContent").style.borderTop = "1px solid #d1d6d9";
	document.getElementById("headerContent").style.width = "100%";
	document.getElementById("headerContent").style.margin = "15px 0 0 0";
	document.getElementById("headerContent").style.textAlign = "center";
	document.getElementById("headerContent").style.textTransform = "uppercase";
	document.getElementById("headerContent").style.letterSpacing = "0.1em";
	document.getElementById("headerContent").style.fontWeight = "100";

	//Footer
	var footer = document.createElement('div');
	var footerContent = document.createElement('div');
	var p = document.createElement('p');
	var content = document.querySelector('.content');
	footerContent.appendChild(p);
	footer.appendChild(footerContent);
	footer.setAttribute("id", "footer");
	footerContent.setAttribute("class", "wrap");
	p.setAttribute("class", "copyright");
	content.parentNode.insertBefore(footer, content.nextSibling);
	document.getElementsByClassName("copyright")[0].innerHTML = "&copy; 2018 Limited Supply. All rights reserved.";
	document.getElementById("footer").style.backgroundColor = "#1c223f";
	document.getElementById("footer").style.color = "#FFF";
	document.getElementById("footer").style.textAlign = "center";
	document.getElementsByClassName("copyright")[0].style.margin = "60px 0 15px 0";
  }
}